<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<html ng-app="drs">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>


    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Minovate - Admin Dashboard</title>
    <link rel="icon" type="image/ico" href="resources/assets/images/favicon.ico" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">




    <!-- ============================================
    ================= Stylesheets ===================
    ============================================= -->
    <!-- vendor css files -->
    <link rel="stylesheet" href="resources/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="resources/assets/css/vendor/animate.css">
    <link rel="stylesheet" href="resources/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/animsition/css/animsition.min.css">

    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/datatables.bootstrap.min.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/extensions/Responsive/css/dataTables.responsive.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/extensions/ColVis/css/dataTables.colVis.min.css">
    <link rel="stylesheet" href="resources/assets/js/vendor/datatables/extensions/TableTools/css/dataTables.tableTools.min.css">
    <link rel="stylesheet" href="resources/assets/css/vendor/toastr/ngToast.min.css">

    <!-- project main css files -->
    <link rel="stylesheet" href="resources/assets/css/main.css">
    <!--/ stylesheets -->



    <!-- ==========================================
    ================= Modernizr ===================
    =========================================== -->
    <script src="resources/assets/js/vendor/modernizr/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <!--/ modernizr -->




</head>





<body id="minovate" class="appWrapper">
<toast></toast>




<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->












<!-- ====================================================
================= Application Content ===================
===================================================== -->
<div id="wrap" class="animsition">






<!-- ===============================================
================= HEADER Content ===================
================================================ -->
<section id="header">
<header class="clearfix">

<!-- Branding -->
<div class="branding">
    <%--<a class="brand" href="index.html">
        <span><strong>MIN</strong>OVATE</span>
    </a>--%>
    <a href="#" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
</div>
<!-- Branding end -->



<!-- Left-side navigation -->
<ul class="nav-left pull-left list-unstyled list-inline">
    <li class="sidebar-collapse divided-right">
        <a href="#" class="collapse-sidebar">
            <i class="fa fa-outdent"></i>
        </a>
    </li>

</ul>
<!-- Left-side navigation end -->
<!-- Right-side navigation -->
<ul class="nav-right pull-right list-inline">
<li class="dropdown nav-profile">

    <a href class="dropdown-toggle" data-toggle="dropdown">
        <img src="resources/assets/images/profile-photo.jpg" alt="" class="img-circle size-30x30">
        <span>John Douey <i class="fa fa-angle-down"></i></span>
    </a>

    <ul class="dropdown-menu animated littleFadeInRight" role="menu">
        <li>
            <a href="logout">
                <i class="fa fa-sign-out"></i>Logout
            </a>
        </li>

    </ul>

</li>
</ul>
<!-- Right-side navigation end -->



</header>

</section>
<!--/ HEADER Content  -->





<!-- =================================================
================= CONTROLS Content ===================
================================================== -->
<div id="controls">





<!-- ================================================
================= SIDEBAR Content ===================
================================================= -->
<aside id="sidebar">


<div id="sidebar-wrap">

<div class="panel-group slim-scroll" role="tablist">
<div class="panel panel-default">
    <div class="panel-heading" role="tab">
        <h4 class="panel-title">
            <img src="resources/assets/images/logo.png"/>
        </h4>
    </div>
    <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
        <div class="panel-body">


            <!-- ===================================================
            ================= NAVIGATION Content ===================
            ==================================================== -->
            <ul id="navigation">
                <li><a href="#/"><i class="fa fa-dashboard"></i> <span>კატალოგი</span></a></li>
                <li><a href="#/users"><i class="fa fa-user"></i> <span>მომხმარებლის მართვა</span></a></li>


            </ul>
            <!--/ NAVIGATION Content -->


        </div>
    </div>
</div>

</div>

</div>


</aside>
<!--/ SIDEBAR Content -->








</div>
<!--/ CONTROLS Content -->




<!-- ====================================================
================= CONTENT ===============================
===================================================== -->
    <div class="view-container" ui-view-container>
        <section id="content" ui-view autoscroll="false">

           

        </section>
    </div>
<!--/ CONTENT -->






</div>
<!--/ Application Content -->

<!-- ============================================
============== Vendor JavaScripts ===============
============================================= -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    window.jQuery || document.write('<script src="resources/assets/js/vendor/jquery/jquery-1.11.2.min.js"><\/script>')
</script>

<script src="resources/assets/js/vendor/bootstrap/bootstrap.min.js"></script>

<script src="resources/assets/js/vendor/jRespond/jRespond.min.js"></script>

<script src="resources/assets/js/vendor/sparkline/jquery.sparkline.min.js"></script>

<script src="resources/assets/js/vendor/slimscroll/jquery.slimscroll.min.js"></script>

<script src="resources/assets/js/vendor/animsition/js/jquery.animsition.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.14.3/ui-bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.14.3/ui-bootstrap-tpls.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.0-rc.0/angular-sanitize.js"></script>
<script src="resources/assets/js/vendor/toastr/ngToast.min.js"></script>




<!-- ============================================
============== Custom JavaScripts ===============
============================================= -->
<script src="resources/assets/js/main.js"></script>

<!--  APP -->
<script src="resources/assets/app.js"></script>
<script src="resources/assets/angular/controller/MainController.js"></script>
<script src="resources/assets/angular/controller/UserController.js"></script>


<script src="resources/assets/angular/service/UserService.js"></script>
<script src="resources/assets/angular/service/gridManager.js"></script>
<script src="resources/assets/angular/service/httpInterceptor.js"></script>
<script src="resources/assets/angular/service/modalManager.js"></script>

<script src="resources/assets/angular/directive/searchInput.js"></script>
<script src="resources/assets/angular/directive/pager.js"></script>



</body>
</html>
