
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="modal fade" id="addEditUserDialog" role="dialog" aria-hidden="true">
    <div class="modal-dialog" ng-form="addEditUserForm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">მომხმარებლის {{init.action == 'Add' ? 'დამატება' : 'რედაქტირება'}} User</h3>
            </div>
        <div class="modal-body">
            <form name="userForm" ng-submit="submitForm(userForm.$valid)" role="form" novalidate>

                    <!-- tile body -->
                    <div class="tile-body">

                        <p class="text-muted">გთხოვთ შეიყვანეთ აუცილებელი ველები</p>

                        <div class="row">

                            <div class="form-group col-md-6" ng-class="{ 'has-error' : userForm.firstName.$invalid && !userForm.firstName.$pristine, 'has-success' : userForm.firstName.$valid && !userForm.firstName.$pristine}">
                                <label for="firstName">სახელი: </label>
                                <input type="text" name="firstName" id="firstName" class="form-control" ng-model="user.firstName" ng-minlength="2" required>
                                <p class="help-block">
                                    <span ng-show="userForm.firstName.$invalid && !userForm.firstName.$pristine">გთხოვთ შეიყვანოთ მომხმარებლის სახელი.</span>
                                    <span ng-show="userForm.firstName.$error.minlength">მომხმარებლის სახელი უნდა შეიცავდეს მინიმუმ 2 სიმბოლოს</span>
                                </p>
                            </div>

                            <div class="form-group col-md-6" ng-class="{ 'has-error' : userForm.lastName.$invalid && !userForm.lastName.$pristine, 'has-success' : userForm.lastName.$valid && !userForm.lastName.$pristine}">
                                <label for="lastName">გვარი: </label>
                                <input type="text" name="lastName" id="lastName" class="form-control" ng-model="user.lastName" ng-minlength="3" required>
                                <p class="help-block">
                                    <span ng-show="userForm.lastName.$invalid && !userForm.lastName.$pristine">გთხოვთ შეიყვანოთ მომხმარებლის გვარი.</span>
                                    <span ng-show="userForm.lastName.$error.minlength">მომხმარებლის გვარი უნდა შეიცავდეს მინიმუმ 3 სიმბოლოს</span>
                                </p>
                            </div>

                        </div>


                        <div class="form-group" ng-class="{ 'has-error' : userForm.email.$invalid && !userForm.email.$pristine, 'has-success' : userForm.email.$valid && !userForm.email.$pristine}">
                            <label for="email">ფოსტა: </label>
                            <input type="email" name="email" id="email" class="form-control" ng-model="user.email" required>
                            <p class="help-block">
                                <span ng-show="userForm.email.$invalid && !userForm.email.$pristine">აუცილებელი ველი.</span>
                            </p>
                        </div>

                        <div class="row" ng-if="init.action == 'Add'">

                            <div class="form-group col-md-6" ng-class="{ 'has-error' : userForm.password.$invalid && !userForm.password.$pristine, 'has-success' : userForm.password.$valid && !userForm.password.$pristine}">
                                <label for="password">პაროლი: </label>
                                <input type="password" name="password" id="password" class="form-control" ng-model="user.password" ng-minlength="6" required>
                                <p class="help-block">
                                    <span ng-show="userForm.password.$invalid && !userForm.password.$pristine">Password is required.</span>
                                    <span ng-show="userForm.password.$error.minlength">Password is too short.</span>
                                </p>
                            </div>

                            <div class="form-group col-md-6" ng-class="{ 'has-error' : userForm.passwordConfirm.$invalid && !userForm.passwordConfirm.$pristine, 'has-success' : userForm.passwordConfirm.$valid && !userForm.passwordConfirm.$pristine}">
                                <label for="passwordConfirm">პაროლი განმეორებით: </label>
                                <input type="password" name="passwordConfirm" id="passwordConfirm" class="form-control" ng-model="user.passwordConfirm" ui-validate=" '$value==user.password' " ui-validate-watch=" 'user.password' " required>
                                <p class="help-block">
                                    <span ng-show='userForm.passwordConfirm.$error.validator'>Passwords do not match!</span>
                                </p>
                            </div>

                        </div>

                    </div>
                    <!-- /tile body -->

                </section>
                <!-- /tile -->


            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" ng-disabled="addEditUserForm.$invalid" ng-click="saveUser()"><i class="fa fa-arrow-right"></i> შენახვა</button>
            <button data-dismiss="modal" class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c"><i class="fa fa-arrow-left"></i> დახურვა</button>
        </div></div></div>
</div>