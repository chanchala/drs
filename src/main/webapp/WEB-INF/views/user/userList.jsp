<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="addEditUser.jsp"%>
<div class="page page-tables-datatables">
	<div class="pageheader">
		<h2>
			{{title}}<span>// {{subTitle}}</span>
		</h2>
	</div>

	<div class="row">
		<!-- col -->
		<div class="col-md-12">

			<!-- tile -->
			<section class="tile">

				<!-- tile header -->
				<div class="tile-header dvd dvd-btm">
					<h1 class="custom-font">
						<strong>მომხმარებლის</strong> სია
					</h1>
					<ul class="controls">
						<li><a role="button" tabindex="0"
							data-options="splash-2 splash-ef-9"
							ng-click="showAddEditUser()()"><i class="fa fa-plus mr-5"></i>
								დამატება</a></li>
					</ul>
				</div>
				<!-- /tile header -->

				<!-- tile body -->
				<div class="tile-body">
					<div class="table-responsive">
						<div id="editable-usage_wrapper"
							class="dataTables_wrapper form-inline dt-bootstrap no-footer">
							<search-input></search-input>
							<div class="row">
								<div class="col-sm-12">
									<table class="table table-custom dataTable no-footer"
										id="editable-usage" role="grid"
										aria-describedby="editable-usage_info">
										<thead>
											<tr role="row">
												<th class="sorting" style="width: 177px;">სახელი</th>
												<th class="sorting" style="width: 207px;">გვარი</th>
												<th class="sorting" style="width: 133px;">ფოსტა</th>
												<th style="width: 160px;" class="no-sort sorting_disabled"></th>
											</tr>
										</thead>
										<tbody>

											<tr ng-repeat="user in data.results" class="gradeX odd"
												role="row">
												<td>{{user.firstName}}</td>
												<td>{{user.lastName}}</td>
												<td>{{user.email}}</td>
												<td class="actions"><a role="button" ng-click="showAddEditUser(user)"
													class="edit text-primary text-uppercase text-strong text-sm mr-10">რედაქტირება</a>
													<a role="button" ng-click="deleteUser(user.id)"
													class="delete text-danger text-uppercase text-strong text-sm mr-10">წაშლა</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								
							<div class="col-sm-12">
							 	<drs-pager></drs-pager>
							</div> 
								
							</div>
						</div>
					</div>
				</div>
				<!-- /tile body -->

			</section>
			<!-- /tile -->

		</div>
		<!-- /col -->
	</div>
</div>