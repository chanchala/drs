angular.module('drs').service('GridManager', function($http) {

    this.givePowerTo =  function($scope, pageSize) {
		$scope.DRSTable = {};
		if (pageSize != undefined) {
			$scope.DRSTable.pageSize = pageSize;
		} else {
			$scope.DRSTable.pageSize = 10;
	    } 
		$scope.DRSTable.openPage = this.openPage;
		$scope.DRSTable.pageChanged = this.pageChanged;
		$scope.DRSTable.sortBy = this.sortBy;
		$scope.DRSTable.reloadData = this.reloadData;
		$scope.DRSTable.textSearch = this.textSearch;
		$scope.DRSTable.textSearchExecute = this.textSearchExecute;
        $scope.DRSTable.pageSetUp = this.pageSetUp;
        $scope.DRSTable.scope = $scope;
	};
	
	this.pageChanged = function(page) {
		var table = this.scope;
		
		table.DRSTable.openPage(page-1);
	};
	
	this.openPage = function(page) {
		var table = this.scope;
		
		table.DRSTable.page = page;
		table.DRSTable.currentPage = page+1;
		
		table.DRSTable.reloadData();
	};
	
	this.textSearch = function(typeTimeOut) {
		var table = this.scope;
		if (typeTimeOut == undefined) {
			typeTimeOut = 300;
		}
		
		table.DRSTable.lastTypeTime = (new Date()).getTime();
		var searchStr = angular.copy(table.DRSTable.searchString);
		setTimeout(function() {
			table.DRSTable.textSearchExecute(table, typeTimeOut, searchStr); 
		}, typeTimeOut + 50);		
	};
	
	this.textSearchExecute = function(table, typeTimeOut, searchString) {
		if (this.searchString != searchString) return ;
		var curTime = (new Date()).getTime();

		if (curTime - table.DRSTable.lastTypeTime >= typeTimeOut) {
			table.DRSTable.page = 0;
			table.DRSTable.currentPage = 1;
			table.DRSTable.reloadData();
		}
	};
	
	this.sortBy = function(sortBy, sender) {
		var table = this.scope;
		if (table.DRSTable.sortColumn == sortBy) {
			table.DRSTable.sortDir = table.DRSTable.sortDir == 'asc' ? 'desc' : 'asc';
		} else {
			table.DRSTable.sortColumn = sortBy;
			table.DRSTable.sortDir = 'asc';
		}
		
		table.DRSTable.page = 0;
		table.DRSTable.currentPage = 1;
		
		table.DRSTable.reloadData();
	};

    this.pageSetUp = function(){
        pageSetUp();
    }

	this.reloadData = function() {
		var table = this.scope;
		var query = [];

		query.pageNumber = table.DRSTable.page>=0?table.DRSTable.page:0;
		query.sortField = table.DRSTable.sortColumn;
		query.isAscending = (table.DRSTable.sortDir == 'asc');
		query.searchExpression = table.DRSTable.searchString;
		query.pageSize = table.DRSTable.pageSize;
		
		if (table.DRSTable.getCustomFilters != undefined) {
			var customFilters = table.DRSTable.getCustomFilters();
			for (var key in customFilters) {
				query[key] = customFilters[key];
			}
		}

	  	$http({url : table.url, 
	  		   method : 'GET',
	  		   params : query}).success(function(data) {

	  	    if (data.results && data.results.length == 0 && table.DRSTable.page > 0) {
	  	    	table.DRSTable.page = 0;
	  	    	table.DRSTable.reloadData();
	  	    	if(data.results && data.results.length == 0){
	  	    		
	  	    	}
	  	    }else if( data.results && data.results.length == 0){
	  	    
	  	    	table.DRSTable.page = -1;
	  	    	table.DRSTable.totalItems = 0;
	  	    	table.data = data;
	  	    } else {
		  		table.data = data;
		  		table.DRSTable.totalItems = data.maxPages * table.DRSTable.pageSize;
		  		if (table.DRSTable.doOnReload != undefined) {
		  			table.DRSTable.doOnReload();
		  		}
		  		if (table.DRSTable.refreshBulkActions != undefined) {
		  			table.DRSTable.refreshBulkActions();
		  		}
	  	    }
	  	    
	  	  table.DRSTable.currentPage = table.DRSTable.page+1;
	  	});
	};

});