angular.module('drs').service('ModalManager', function($rootScope, $sce) {
    this.enableModals =  function($scope) {
        $scope.showConfirmModal = this.showConfirmModal;
        $scope.showMessageModal = this.showMessageModal;
        $scope.showErrorModal = this.showErrorModal;
        $scope.convertErrorHint = this.convertErrorHint;
        $scope.showSuccessAlert = this.showSuccessAlert;
        $scope.show = this.show;
        $scope.hide = this.hide;
        $scope.showCustomPage = this.showCustomPage;
        $scope.hideCustomPage = this.hideCustomPage;
    };

    this.showConfirmModal = function(title, message, doOnModalConfirm, doOnModalCancel) {
        var element = "#confirmModal";

        var scope = this;

        $rootScope.modalTitle = title;
        $rootScope.modalBody =  message ? $sce.trustAsHtml(message) : message;
        $rootScope.doOnModalConfirm = function() {
            if (doOnModalConfirm != undefined) {
                doOnModalConfirm(scope);
            }
        };

        $rootScope.doOnModalCancel = function() {
            if (doOnModalCancel != undefined) {
                doOnModalCancel(scope);
            }
        };

        scope.show(element);
    };

    this.showMessageModal = function(title, message, doOnModalOK) {
        var scope = this;

        $rootScope.modalTitle = title;
        $rootScope.modalBody = message ? $sce.trustAsHtml(message) : message;
        $rootScope.doOnModalOK = function() {
            if (doOnModalOK != undefined) {
                doOnModalOK(scope);
            }
        };

        scope.show('#messageModal');
    };


    // error hint messes up our css for h1, b. so I replaced them with custom tag names
    this.convertErrorHint = function(errorHint){
        errorHint = errorHint == null ? "" : errorHint;
        errorHint = errorHint.replace('H1', 'HX');
        errorHint = errorHint.replace('h1', 'HX');
        errorHint = errorHint.replace('B {', 'BX {');
        errorHint = errorHint.replace('<b>', '<BX>');
        errorHint = errorHint.replace('</b>', '</BX>');
        errorHint = errorHint.replace('A {', '#errorHintHtml A {');

        return errorHint;
    };

    this.showErrorModal = function(message, errorHint, doOnModalOK, httpStatus) {
        var scope = this;
        $rootScope.stackTrace = true;
        $rootScope.stackTraceTitle = "Show Stack Trace";
        if (httpStatus != undefined) {
            $rootScope.httpStatus = "HTTP " + httpStatus;
        } else {
            $rootScope.httpStatus = "";
        }
        $rootScope.stackTraceAvailable = (errorHint != null && errorHint != undefined && errorHint.trim() != '');

        $rootScope.errorHint = errorHint ? scope.convertErrorHint(errorHint) : "";

        $('#errorModalBody').html( message );
        $('#errorHintHtml').html( $rootScope.errorHint );

        $rootScope.doOnModalOK = function() {
            if (doOnModalOK != undefined) {
                doOnModalOK(scope);
            }
        };

        $( "#errorHintHtml" ).hide();

        $rootScope.showStackTrace = function(action) {
            if(action){
                $rootScope.stackTrace = false;
                $rootScope.stackTraceTitle = "Hide Stack Trace";
                $( "#errorHintHtml" ).show();
            } else {
                $rootScope.stackTrace = true;
                $rootScope.stackTraceTitle = "Show Stack Trace";
                $( "#errorHintHtml" ).hide();
            }
        };

        scope.show('#errorModal');
    };

    this.showSuccessAlert = function(title, message) {

        var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: title,
            // (string | mandatory) the text inside the notification
            text: message,
            // (string | optional) the image to display on the left
            image: 'assets/images/accepted.png',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
        });

        // You can have it return a unique id, this can be used to manually remove it later using
        setTimeout(function() {
            $.gritter.remove(unique_id, {
                fade: true,
                speed: 'slow'
            });

        }, 5000);
    };

    this.show = function(element){
        if($(element).data('bs.modal') && $(element).data('bs.modal').$backdrop){
            // When closing and opening immediately a modal window gets stuck,
            // thats why we need to add some timeout to fix the issue
            setTimeout(function(){

            }, 200);

            $(element).on('hidden.bs.modal', function () {
                if($(element).parent().prop("tag") != "body" && !$(element).hasClass("common-modal")){
                    $(element).appendTo("body").modal("show");
                } else {
                    $(element).modal("show");
                }

                $(element).off('hidden.bs.modal');
            });

            return;
        }

        if($(element).parent().prop("tag") != "body" && !$(element).hasClass("common-modal")){
            $(element).appendTo("body").modal("show");
        } else {
            $(element).modal("show");
        }
    };

    this.hide = function(element) {
        $(element).modal("hide");
    };
});