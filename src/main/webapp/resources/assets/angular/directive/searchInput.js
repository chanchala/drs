angular.module('drs').directive('searchInput', function() {
	return {
		restrict : 'E',
		templateUrl : 'resources/customTemplates/SearchInput.html'
	};
});