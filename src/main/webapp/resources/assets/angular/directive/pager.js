angular.module('drs').directive('drsPager', function() {
	return {
		restrict : 'E',
		templateUrl : 'resources/customTemplates/Pager.html'
	};
});