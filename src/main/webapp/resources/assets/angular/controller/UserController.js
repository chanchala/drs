angular.module('drs').controller('UserController',
    [ '$scope', 'UserService','GridManager', 'ModalManager','$http','ngToast', function($scope, UserService, GridManager, ModalManager,$http, ngToast){
    $scope.title = 'მომხმარებლის მართვა';
    $scope.subTitle = 'დამატებითი სათაური';
    GridManager.givePowerTo($scope);
    ModalManager.enableModals($scope);
    $scope.url = 'user/list';
    $scope.DRSTable.openPage(0);
    $scope.init = {};

    $scope.showAddEditUser = function (user) {
        $scope.init.action = user ? 'Edit' : 'Add';
        if(user){
            $scope.user = angular.copy(user);
        }else{
            $scope.user = {};
        }
        $('#addEditUserDialog').modal('show');
    };

    $scope.saveUser = function (user) {
        var userCopy = angular.copy($scope.user);
        delete userCopy.passwordConfirm;
        UserService.addEditUser(userCopy).then(function(response) {
            if (response.isSuccess) {
                ModalManager.hide('#addEditUserDialog');
                $scope.DRSTable.reloadData();
                ngToast.create('მომხმარებელი წარმატებით დაემატა');
            } else {
                $scope.showErrorModal(response.errorMessage, response.exceptionMessage);
            }
            $scope.user = null;
        });
    };
    
    $scope.deleteUser = function (userId) {
    	UserService.deleteUser(userId).then(function(response) {
            if (response.isSuccess) {
                $scope.DRSTable.reloadData();
                ngToast.create('მომხმარებელი წარმატებით წაიშალა');
            } else {
                $scope.showErrorModal(response.errorMessage, response.exceptionMessage);
            }
            
        });
	};

}]);