package com.creative.merge.controller;

import com.creative.merge.dto.UserDTO;
import com.creative.merge.model.User;
import com.creative.merge.service.UserService;
import com.creative.merge.utils.PaginationAndFullSearchQueryResult;
import com.creative.merge.utils.RequestResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value="/user" )
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/getTemplate" )
	public String getTemplate() {
		return "user/userList";
	}

    @RequestMapping("/list" )
    @ResponseBody
    public PaginationAndFullSearchQueryResult<UserDTO> getList(
            @RequestParam(required = false, defaultValue = "") String searchExpression,
            @RequestParam(required = false, defaultValue = "") String sortField,
            @RequestParam(required = false, defaultValue = "true") boolean isAscending,
            @RequestParam(value = "pageNumber", required = false, defaultValue = "0") Integer pageNumber,
            @RequestParam(required = false, defaultValue = "10") int pageSize) {
        return userService.getList(searchExpression, sortField, isAscending, pageNumber, pageSize);

    }
    
    @RequestMapping(value="/saveUser", method=RequestMethod.POST )
    @ResponseBody
    public RequestResponse saveUser(@RequestBody User user) {
    	userService.saveUser(user);
        return RequestResponse.SUCCESS;

    }
    
    @RequestMapping(value = "/delete")
    @ResponseBody
	public RequestResponse delete (@RequestParam(required = false, defaultValue = "") Long id) {
    		userService.delete(id);
		return RequestResponse.SUCCESS;
	}

}


