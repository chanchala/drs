package com.creative.merge.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@MappedSuperclass
public class KingEntity implements Serializable {

    @Column(name="CREATED_AT")
    private Date createdAt;

    @Column(name="UPDATED_AT")
    private Date updatedAt;
    
    @PrePersist
    void prePersist() {
        this.createdAt = this.updatedAt = new Date();
    }

    @PreUpdate
    void preUpdate() {
        this.updatedAt = new Date();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

 	public boolean isCreatedBeforeDate(Date now) {
        return createdAt != null && createdAt.before(now);
    }
    public boolean isUpdatedBeforeDate(Date now) {
        return updatedAt != null && updatedAt.before(now);
    }

	
    
    

}