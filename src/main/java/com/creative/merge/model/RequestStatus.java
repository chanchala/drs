package com.creative.merge.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 4:41 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "REQUEST_STATUS")
public class RequestStatus extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String requestName;
    
    @Column(name = "COLOR", nullable = false)
    private String color;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
    
    
}