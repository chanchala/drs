package com.creative.merge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 5:10 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * @author User
 *
 */
@Entity
@Table(name = "REQUEST_FORM")
public class RequestForm extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FORM_NAME", nullable = false)
    private String formName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

    
}
