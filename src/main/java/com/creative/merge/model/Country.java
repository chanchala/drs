package com.creative.merge.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: g_arc_000
 * Date: 12/5/15
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "COUNTRY")
public class Country extends KingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "COUNTRY_NAME", nullable = false)
    private String countryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
