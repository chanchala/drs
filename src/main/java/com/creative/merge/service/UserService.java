package com.creative.merge.service;



import com.creative.merge.dao.UserDAO;
import com.creative.merge.dto.UserDTO;
import com.creative.merge.model.User;
import com.creative.merge.utils.PaginationAndFullSearchQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Transactional(readOnly = true)
    public PaginationAndFullSearchQueryResult<UserDTO> getList(String searchExpression, String sortField, boolean isAscending, Integer pageNumber, int pageSize) {
        return userDAO.getPaginatedResultList(User.class, searchExpression,
                sortField, isAscending, pageNumber, pageSize).transform(UserDTO.class);
    }

    @Transactional(readOnly = true)
    public List<User> getCustomList() {
        return userDAO.getCustomList();
    }
    
    @Transactional
    public User saveUser(User user) {
        return userDAO.saveUser(user);
    }
    
    @Transactional
    public void delete(Long id) {
        userDAO.delete(id);
    }

}
