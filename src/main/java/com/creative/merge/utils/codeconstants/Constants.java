package com.creative.merge.utils.codeconstants;

/**
 * Created by TuraMan on 5/13/2015.
 */
public class Constants {

    /**
     * Error Code Constants.
     * Starting from 1000 till 2000
     */
    public static final class ErrorCodes{
        public static final int INVALID_USERNAME_OR_PASSWORD = 1000;
        public static final int YOU_DO_NOT_HAVE_PERMISSION_TO_PERFORM_THIS_ACTION = 1001;

        public class ErrorResponse{
            public static final String EMPTY_NOT_ALLOWED = "parameter could not be empty";
        }
    }

    /**
     * SuccessMessages Code Constants.
     * Starting from 2000 till 3000
     */
    public static final class SuccessMessages{
        public static final int LOGGED_OUT_SUCCESSFULLY = 3000;
        public static final int OPERATION_COMPLETED_SUCCESSFULLY = 3001;
    }

    /**
     * CustomCodeConstants code Constants
     * This class contains all the string constants, needed in the classes while coding something
     */
    public static final class CustomCodeConstants{

        public static final String ERROR = "error";
        public static final String LOGOUT = "logout";
        public static final String LOGIN = "login";
        public static final String SLASH = "/";
        public static final String MSG = "msg";
        public static final String SUCCESS = "success";
        public static final String CODE = "code";
        public static final String SOURCE = "source";
        public static final String MESSAGE = "message";
        public static final String RESOURCES_ALL = "/resources/**";
        public static final String STRING_EMPTY = "";
        public static final String UNKNOWN_ERROR = "Something went wrong!";


        public static class Keys{

            public static final String PAGE_NUMBER = "pageNumber";
            public static final String PAGE_NUMBER_DEFAULT_VALUE = "0";
            public static final String PAGE_SIZE_DEFAULT_VALUE = "10";
            public static final String IS_ASCENDING_DEFAULT_VALUE = "true";
            public static final String LIST = "list";
            public static final String APP = "application";
            public static final String LAYOUT = "layout";
            public static final String SAVE = "save";
            public static final String DELETE = "delete";
            public static final String ROLE_ID = "RoleId";
            public static final String PERMISSIONS = "Permissions";
            public static final String KEY = "key";
            public static final String VALUE = "value";


        }
    }

    public static final class HTTPCodes{
        public static final int HTTP_OK = 200;
        public static final int HTTP_UNAUTHORIZED = 401;
        public static final int HTTP_BAD_REQUEST = 400;
        public static final int HTTP_NOTFOUND = 404;
        public static final int HTTP_TIMEOUT = 408;
        public static final int HTTP_CANT_CONTACT_SERVER = 407;
        public static final int HTTP_SERVER_ERROR = 500;
        public static final int DEFTIMEOUT = 60;
        public static final int DEFREFCOUNT = 10;
        public static final int DEFRETRYPERIOD = 30;
    }

}
