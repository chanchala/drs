package com.creative.merge.config;

import com.creative.merge.utils.codeconstants.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

import static com.creative.merge.utils.codeconstants.Constants.*;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
	@Autowired
	@Qualifier("userDetailsService")
	UserDetailsService userDetailsService;

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .csrf().disable()
            .authorizeRequests()
                .antMatchers(CustomCodeConstants.RESOURCES_ALL).permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                .loginPage(CustomCodeConstants.SLASH + CustomCodeConstants.LOGIN)
                .permitAll()
                .and()
            .logout().logoutSuccessUrl(CustomCodeConstants.SLASH);
    }
 
	@Autowired
    public void configureGlobal(UserDetailsService userDetailsService, AuthenticationManagerBuilder auth ) throws Exception{
		auth.userDetailsService(userDetailsService);
	}
 
}
