package com.creative.merge.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@EnableWebMvc
@Configuration
@ComponentScan({"com.creative.merge.*"})
@EnableTransactionManagement
@Import({ SecurityConfig.class })

public class AppConfig extends WebMvcConfigurerAdapter{
 
        /*@Bean
        public SessionFactory sessionFactory() {
                LocalSessionFactoryBuilder builder = 
			new LocalSessionFactoryBuilder(dataSource());
                builder.scanPackages("com.ids.model")
                      .addProperties(getHibernateProperties());
 
                return builder.buildSessionFactory();
        }*/
        
        @Bean
    	public LocalContainerEntityManagerFactoryBean entityManagerFactory()
    	{
    		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();

    		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    		vendorAdapter.setGenerateDdl(Boolean.TRUE);
    		vendorAdapter.setShowSql(Boolean.TRUE);

    		factory.setDataSource(dataSource());
    		factory.setJpaVendorAdapter(vendorAdapter);
    		factory.setPackagesToScan("com.creative.merge.model");

    		Properties jpaProperties = getHibernateProperties();
    		factory.setJpaProperties(jpaProperties);

    		factory.afterPropertiesSet();
    		factory.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
    		return factory;
    	}
 
	private Properties getHibernateProperties() {
                Properties prop = new Properties();
                prop.put("hibernate.format_sql", "true");
                prop.put("hibernate.hbm2ddl.auto", "create-drop");
                //prop.put("hibernate.hbm2ddl.auto", "update");
                prop.put("hibernate.hbm2ddl.import_files","sql/import.sql");
                prop.put("hibernate.show_sql", "true");
                prop.put("hibernate.dialect", 
                    "org.hibernate.dialect.MySQL5Dialect");
                return prop;
        }
 
	@Bean(name = "dataSource")
	public BasicDataSource dataSource() {
 
		BasicDataSource ds = new BasicDataSource();
	        ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/drs?useUnicode=true&amp;connectionCollation=utf8_general_ci&amp;characterSetResults=utf8&amp;characterEncoding=utf8");
		ds.setUsername("root");
		ds.setPassword("");
		return ds;
	}
 
	//Create a transaction manager
	@Bean
	public PlatformTransactionManager transactionManager()
	{
		EntityManagerFactory factory = entityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}
 
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver 
                             = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
}